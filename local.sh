#!/bin/bash

# For loop 
for i in `seq 1 $1`
do  
  #172.217.3.206
  #216.58.195.238
  ping -c $2 $3 >> raw_data.txt & # Put a function in the background
done 

wait
echo "All done"
