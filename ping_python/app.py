import os
import subprocess
import csv

result = open("result_docker.csv", "a")
csvwr = csv.writer(result)
outcome = subprocess.check_output(["ping","-c", "50", "google.com", "-4"])
outcome = outcome.split("\n")
csvwr.writerow(["Size", "", "", "Domain", "IPv4", "Sequence", "TTL", "Latency"])
for i in outcome:
    if "PING" not in i and "ping" not in i:
        i = i.split()
        for j in i:
            if "(" in j:
                j = j[1:-2]
                i[4] = j
            elif "icmp_seq" in j:
                j = j.split("=")
                j = j[1]
                i[5] = j
            elif "ttl" in j:
                j = j.split("=")
                j = j[1]
                i[6] = j
            elif "time=" in j:
                j = j.split("=")
                j = j[1]
                i[7] = j
        csvwr.writerow(i)
result.close()
