#!/bin/bash
#Usage: sh usage_data_parsing.sh


# for directory in docker/Mar.1*; do
# 	for sub_directory in "$directory"/*/; do
# 		touch "$sub_directory"/cpu_usage.csv
# 		touch "$sub_directory"/task_usage.csv
# 		touch "$sub_directory"/mem_usage.csv
# 		cat "$sub_directory"/raw_usage_data.txt | grep %Cpu | cut -c 36-41 | nl > "$sub_directory"/cpu_usage.csv
# 		cat "$sub_directory"/raw_usage_data.txt | grep Tasks: | cut -c 8-11 | nl > "$sub_directory"/task_usage.csv
# 		cat "$sub_directory"/raw_usage_data.txt | grep buff/cache | cut -c 43-50 | nl > "$sub_directory"/mem_usage.csv

# 	done
# done

# for directory in local/Mar.1*; do
# 	for sub_directory in "$directory"/*/; do
# 		touch "$sub_directory"/cpu_usage.csv
# 		touch "$sub_directory"/task_usage.csv
# 		touch "$sub_directory"/mem_usage.csv
# 		cat "$sub_directory"/raw_usage_data.txt | grep %Cpu | cut -c 36-41 |nl > "$sub_directory"/cpu_usage.csv
# 		cat "$sub_directory"/raw_usage_data.txt | grep Tasks: | cut -c 8-11 | nl > "$sub_directory"/task_usage.csv
# 		cat "$sub_directory"/raw_usage_data.txt | grep buff/cache | cut -c 43-50 | nl > "$sub_directory"/mem_usage.csv
# 	done
# done
echo "docker" >> total_cpu_usage.csv 
echo "docker" >> total_tasks.csv
echo "docker" >> total_mem.csv
for directory in docker/Mar.1*; do
	for sub_directory in "$directory"/*/; do
		echo "$sub_directory" | cut -c 17- >> total_cpu_usage.csv
		cat "$sub_directory"/raw_usage_data.txt | grep %Cpu | cut -c 36-41 | nl >> total_cpu_usage.csv
		echo "$sub_directory" | cut -c 17- >> total_tasks.csv
		cat "$sub_directory"/raw_usage_data.txt | grep Tasks: | cut -c 8-11 | nl >> total_tasks.csv
		echo "$sub_directory" | cut -c 17- >> total_mem.csv
		cat "$sub_directory"/raw_usage_data.txt | grep buff/cache | cut -c 43-50 | nl >> total_mem.csv
	done
done

echo "local" >> total_cpu_usage.csv 
echo "local" >> total_tasks.csv
echo "local" >> total_mem.csv
for directory in local/Mar.1*; do
	for sub_directory in "$directory"/*/; do
		echo "$sub_directory" | cut -c 16- >> total_cpu_usage.csv
		cat "$sub_directory"/raw_usage_data.txt | grep %Cpu | cut -c 36-41 | nl >> total_cpu_usage.csv
		echo "$sub_directory" | cut -c 16- >> total_tasks.csv
		cat "$sub_directory"/raw_usage_data.txt | grep Tasks: | cut -c 8-11 | nl >> total_tasks.csv
		echo "$sub_directory" | cut -c 16- >> total_mem.csv
		cat "$sub_directory"/raw_usage_data.txt | grep buff/cache | cut -c 43-50 | nl >> total_mem.csv
	done
done