#!/bin/bash
#Usage: sh usage_data_parsing.sh

echo "docker"
for directory in docker/Mar.1*; do
	for sub_directory in "$directory"/*/; do
		echo "$sub_directory" | cut -c 17-| tr "\n" ","
		cat "$sub_directory"/cpu_usage.csv | wc -l 
	done
done

echo "local"
for directory in local/Mar.1*; do
	for sub_directory in "$directory"/*/; do
		echo "$sub_directory" | cut -c 16- | tr "\n" ","
		cat "$sub_directory"/cpu_usage.csv | wc -l 
	done
done
